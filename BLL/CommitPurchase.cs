﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public static class PurchaseHelper
    {
        public static (bool, string) CanCommitPurchase(User user, Product product)
        {
            if (product.Count > 0)
                if(user.CoinsInFM >= product.Cost)
                    return (true, String.Empty);
                else
                    return (false, "Не достаточно средств");
            else
                return (false, "Данный продукт закончился");
        }
    }
}
