﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public interface IAuthenticationManager
    {
        Task<bool> ValidateUser(LoginModel userForAuth);
        Task<string> CreateToken();
    }
}
