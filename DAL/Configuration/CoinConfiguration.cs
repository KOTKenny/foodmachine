﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Configuration
{
    public class CoinConfiguration : IEntityTypeConfiguration<Coin>
    {
        public void Configure(EntityTypeBuilder<Coin> builder)
        {
            builder.HasData(
                new Coin
                {
                    Id = 1,
                    Accept = true,
                    Nominal = 1,
                    Amount = 0
                },
                new Coin
                {
                    Id = 2,
                    Accept = true,
                    Nominal = 2,
                    Amount = 0
                },
                new Coin
                {
                    Id = 3,
                    Accept = true,
                    Nominal = 5,
                    Amount = 0
                }
            );
        }
    }
}
