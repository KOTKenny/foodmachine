﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Configuration
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasData(
                new Product
                {
                    Id = 1,
                    Name = "Coca-Cola",
                    Cost = 1.69M,
                    CreateDate = DateTime.Now,
                    ImgName = "coca-cola.png"
                },
                new Product
                {
                    Id = 2,
                    Name = "Lays 'Сметана и зелень'",
                    Cost = 2.52M,
                    CreateDate = DateTime.Now,
                    ImgName = "lays.png"
                }
            );
        }
    }
}
