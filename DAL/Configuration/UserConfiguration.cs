﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasData(
                new User
                {
                    Id = 1,
                    CoinsInFM = 0,
                    Name = "Илья",
                    CreateDate = DateTime.Now
                },
                new User
                {
                    Id = 2,
                    CoinsInFM = 0,
                    Name = "Саша",
                    CreateDate = DateTime.Now
                }
            );
        }
    }
}
