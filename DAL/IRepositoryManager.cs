﻿using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public interface IRepositoryManager
    {
        IProductRepository Product { get; }
        ISettingRepository Setting { get; }
        IUserRepository User { get; }
        ICoinRepository Coin { get; }
        IPurchaseRepository Purchase { get; }
        void Save();
    }
}
