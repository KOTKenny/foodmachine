﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Coin
    {
        public int Id { get; set; }
        public int Nominal { get; set; }
        public int Amount { get; set; }
        public bool Accept { get; set; }
    }
}
