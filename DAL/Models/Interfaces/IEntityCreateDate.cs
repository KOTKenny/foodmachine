﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models.Interfaces
{
    public interface IEntityCreateDate
    {
        public DateTime CreateDate { get; set; }
    }
}
