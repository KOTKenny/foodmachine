﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Email обязательное поле")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessage = "Пароль обязательное поле")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string UserName { get; set; }
    }
}
