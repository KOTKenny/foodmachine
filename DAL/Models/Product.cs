﻿using DAL.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Product : IEntityCreateDate
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is a required field.")]
        [MaxLength(200, ErrorMessage = "Maximum length for the Name is 200 characters.")]
        public string Name { get; set; }
        public string ImgName { get; set; }
        public decimal Cost { get; set; }
        public int Count { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
