﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Setting
    {
        public int Id { get; set; }
        public bool WorkingNow { get; set; }
        public ICollection<Coin> Coins { get; set; }
    }
}
