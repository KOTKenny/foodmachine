﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class User : IdentityUser<int>
    {
        public string Name { get; set; }
        public decimal CoinsInFM { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
