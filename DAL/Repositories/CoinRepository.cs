﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class CoinRepository : ReposityryBase<Coin>, ICoinRepository
    {
        public CoinRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }
        public Coin GetCoin(int coinId, bool trackChanges) =>
            FindByCondition(c => c.Id.Equals(coinId), trackChanges)
            .SingleOrDefault();

        public Coin GetCoinByNominal(int coinNominal, bool trackChanges) =>
            FindByCondition(c => c.Nominal.Equals(coinNominal), trackChanges)
            .SingleOrDefault();

        public IEnumerable<Coin> GetCoins(bool trackChanges) => FindAll(trackChanges);

        public IEnumerable<Coin> GetCoinsNominalDesc(bool trackChanges) =>
            FindAll(trackChanges)
            .OrderByDescending(c => c.Nominal);

        public void UpdateCoin(Coin coin) => Update(coin);
    }
}
