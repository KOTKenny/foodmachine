﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories.Interfaces
{
    public interface ICoinRepository
    {
        IEnumerable<Coin> GetCoins(bool trackChanges);
        IEnumerable<Coin> GetCoinsNominalDesc(bool trackChanges);
        Coin GetCoin(int coinId, bool trackChanges);
        Coin GetCoinByNominal(int coinNominal, bool trackChanges);
        void UpdateCoin(Coin coin);
    }
}
