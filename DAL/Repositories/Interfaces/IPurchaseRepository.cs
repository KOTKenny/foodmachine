﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories.Interfaces
{
    public interface IPurchaseRepository
    {
        IEnumerable<Purchase> GetPurchasesByUserId(int userId, bool loadProducts, bool trackChanges);
        void CreatePurchase(Purchase purchase);
        //void Update(Purchase purchase);
    }
}
