﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories.Interfaces
{
    public interface ISettingRepository
    {
        Setting GetSetting(int settingId, bool trackChanges);
        void UpdateSetting(Setting setting);
    }
}
