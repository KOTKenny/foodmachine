﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories.Interfaces
{
    public interface IUserRepository
    {
        User GetUser(int id, bool trackChanges);
        void UpdateUser(User user);
    }
}
