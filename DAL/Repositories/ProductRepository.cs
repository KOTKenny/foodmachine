﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class ProductRepository : ReposityryBase<Product>, IProductRepository
    {
        public ProductRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }

        public IEnumerable<Product> GetAllProducts(bool trackChanges) =>
            FindAll(trackChanges)
            .OrderBy(p => p.Name)
            .ToList();

        public Product GetProduct(int productId, bool trackChanges) =>
            FindByCondition(p => p.Id.Equals(productId), trackChanges)
            .SingleOrDefault();

        public void CreateProduct(Product product) =>
            Create(product);

        public void DeleteProduct(Product product) =>
            Delete(product);

        public void UpdateProduct(Product product) =>
            Update(product);

        public IEnumerable<Product> GetProductsByPurchases(IEnumerable<Purchase> purchases, bool trackChanges) =>
            FindByCondition(prod => purchases.Any(purch => purch.ProductId.Equals(prod.Id)), trackChanges);
    }
}
