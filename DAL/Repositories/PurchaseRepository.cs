﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class PurchaseRepository : ReposityryBase<Purchase>, IPurchaseRepository
    {
        public PurchaseRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }

        public void CreatePurchase(Purchase purchase) => Create(purchase);

        public IEnumerable<Purchase> GetPurchasesByUserId(int userId, bool loadProducts, bool trackChanges) =>
            !loadProducts ?
                FindByCondition(p => p.UserId.Equals(userId), trackChanges) :
                FindByCondition(p => p.UserId.Equals(userId), trackChanges).Include(p => p.Product);

    }
}
