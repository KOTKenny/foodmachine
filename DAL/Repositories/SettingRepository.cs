﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class SettingRepository : ReposityryBase<Setting>, ISettingRepository
    {

        public SettingRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }

        public Setting GetSetting(int settingId, bool trackChanges) =>
            FindByCondition(x => x.Id.Equals(settingId), trackChanges)
            .SingleOrDefault();

        public void UpdateSetting(Setting setting) => Update(setting);
    }
}
