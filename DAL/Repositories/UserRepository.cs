﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories.Interfaces
{
    public class UserRepository : ReposityryBase<User>, IUserRepository
    {
        public UserRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }
        public User GetUser(int id, bool trackChanges) =>
            FindByCondition(u => u.Id.Equals(id), trackChanges)
            .SingleOrDefault();

        public void UpdateUser(User user) => Update(user);
    }
}
