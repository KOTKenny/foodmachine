﻿using DAL.Repositories;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class RepositoryManager : IRepositoryManager
    {
        private RepositoryContext _repositoryContext;
        private IProductRepository _productRepository;
        private ISettingRepository _settingRepository;
        private IUserRepository _userRepository;
        private ICoinRepository _coinRepository;
        private IPurchaseRepository _purchaseRepository;

        public RepositoryManager(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public IProductRepository Product
        {
            get
            {
                if(_productRepository == null)
                    _productRepository = new ProductRepository(_repositoryContext);

                return _productRepository;
            }
        }

        public ISettingRepository Setting
        {
            get 
            {
                if (_settingRepository == null)
                    _settingRepository = new SettingRepository(_repositoryContext);

                return _settingRepository;
            }
        }

        public IUserRepository User
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_repositoryContext);

                return _userRepository;
            }
        }

        public ICoinRepository Coin
        {
            get
            {
                if (_coinRepository == null)
                    _coinRepository = new CoinRepository(_repositoryContext);

                return _coinRepository;
            }
        }

        public IPurchaseRepository Purchase
        {
            get
            {
                if (_purchaseRepository == null)
                    _purchaseRepository = new PurchaseRepository(_repositoryContext);

                return _purchaseRepository;
            }
        }

        public void Save() => _repositoryContext.SaveChanges();
    }
}
