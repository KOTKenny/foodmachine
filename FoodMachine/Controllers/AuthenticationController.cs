﻿using AutoMapper;
using BLL.Services;
using DAL.Models;
using DAL.Models.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodMachine.Controllers
{
    [Route("api/[controller]")]
	[AllowAnonymous]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
		private readonly UserManager<User> _userManager;
		private readonly IMapper _mapper;
		private readonly IAuthenticationManager _authManager;

		public AuthenticationController(UserManager<User> userManager, IAuthenticationManager authManager, IMapper mapper)
		{
			_userManager = userManager;
			_authManager = authManager;
			_mapper = mapper;
		}

		[HttpPost("login")]
		public async Task<IActionResult> Authenticate([FromBody] LoginModel user)
		{
			if (!await _authManager.ValidateUser(user))
			{
				return Unauthorized();
			}

			return Ok(new { Token = await _authManager.CreateToken() });
		}


		[HttpPost]
		public async Task<IActionResult> RegisterUser([FromBody] LoginModel userForRegistration)
		{
			var user = _mapper.Map<User>(userForRegistration);
			var result = await _userManager.CreateAsync(user, userForRegistration.Password);

			if (!result.Succeeded)
			{
				foreach (var error in result.Errors)
				{
					ModelState.TryAddModelError(error.Code, error.Description);
				}
				return BadRequest(ModelState);
			}

			return StatusCode(201);
		}

	}
}
