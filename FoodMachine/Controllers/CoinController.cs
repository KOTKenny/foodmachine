﻿using DAL;
using DAL.Models;
using BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodMachine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoinController : ControllerBase
    {
        private IRepositoryManager _repository;

        public CoinController(IRepositoryManager repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult GetCoins()
        {
            var coins = _repository.Coin.GetCoins(trackChanges: false);

            return Ok(coins);
        }
    }
}
