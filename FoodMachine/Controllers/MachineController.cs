﻿using BLL;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodMachine.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class MachineController : ControllerBase
    {
        private IRepositoryManager _repository;

        public MachineController(IRepositoryManager repository)
        {
            _repository = repository;
        }

        [HttpPut("{coinNominal}", Name = "addcoin")]
        public IActionResult AddCoin(int coinNominal, [FromBody]User user)
        {
            if (user == null)
                return BadRequest("User object is null");

            var userEntity = _repository.User.GetUser(user.Id, trackChanges: false);

            if (userEntity == null)
                return NotFound("User not found");

            //userEntity.Role = _repository.Role.GetRole(userEntity.RoleId, trackChanges: false);

            var coin = _repository.Coin.GetCoinByNominal(coinNominal, trackChanges: false);

            if (coin == null)
                return NotFound("Coin not found");

            coin.Amount++;
            userEntity.CoinsInFM += coin.Nominal;

            _repository.Coin.UpdateCoin(coin);
            _repository.User.UpdateUser(userEntity);
            _repository.Save();

            return Ok();
        }

        [HttpPut]
        public IActionResult Withdrawal([FromBody] User user)
        {
            if (user == null)
                return BadRequest("User object is null");

            var userEntity = _repository.User.GetUser(user.Id, trackChanges: false);

            if (userEntity == null)
                return NotFound();

            var coins = _repository.Coin.GetCoinsNominalDesc(trackChanges: false);

            if (MoneyMethods.CanExchangeMoney(userEntity.CoinsInFM, ref coins))
            {
                userEntity.CoinsInFM = 0;
                _repository.User.UpdateUser(userEntity);

                foreach (var coin in coins)
                {
                    _repository.Coin.UpdateCoin(coin);
                }
                _repository.Save();
                return Ok();
            }
            else
            {
                Response.StatusCode = 304;
                return Content(String.Empty);
            }
        }
    }
}
