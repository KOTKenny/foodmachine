﻿using BLL;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FoodMachine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductController : ControllerBase
    {
        private readonly IRepositoryManager _repository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ProductController(IRepositoryManager repository, IHttpContextAccessor httpContextAccessor)
        {
            _repository = repository;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public IActionResult GetProducts()
        {
            var products = _repository.Product.GetAllProducts(trackChanges: false);

            return Ok(products);
        }

        [HttpGet("{id}", Name = "ProductById")]
        public IActionResult GetProduct(int id)
        {
            var product = _repository.Product.GetProduct(id, trackChanges: false);

            return Ok(product);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult CreateProduct([FromBody] Product product)
        {
            if (product == null)
                return BadRequest("Product object in null");

            _repository.Product.CreateProduct(product);
            _repository.Save();

            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteProduct(int id)
        {
            var product = _repository.Product.GetProduct(id, trackChanges: false);

            if (product == null)
                return NotFound();

            _repository.Product.DeleteProduct(product);
            _repository.Save();

            return NoContent();
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateProduct(int id, [FromBody] Product product)
        {
            if (product == null)
                return BadRequest("Product object is null");

            var productEntity = _repository.Product.GetProduct(id, trackChanges: false);

            if (productEntity == null)
                return NotFound();

            product.Id = productEntity.Id;
            _repository.Product.UpdateProduct(product);
            _repository.Save();

            return NoContent();
        }

        [HttpPost("buy/{id}")]
        public IActionResult Buy(int id)
        {

            Claim IdClaim = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Sid);

            if (IdClaim == null)
                return BadRequest("User Id info is null");

            var userEntity = _repository.User.GetUser(int.Parse(IdClaim.Value), trackChanges: false);

            if (userEntity == null)
                return NotFound("User not found!");

            var product = _repository.Product.GetProduct(id, trackChanges: false);

            if (product == null)
                return NotFound("Product not found!");

            (bool isCanCommitPurchase, string msg) = PurchaseHelper.CanCommitPurchase(userEntity, product);

            if (isCanCommitPurchase)
            {
                userEntity.CoinsInFM -= product.Cost;
                _repository.User.UpdateUser(userEntity);
                _repository.Purchase.CreatePurchase(new Purchase { ProductId = product.Id, UserId = userEntity.Id });
                _repository.Save();

                return Ok(new { message = String.Format("Вы купили {0}!", product.Name) });
            }
            else
            {
                return BadRequest(msg);
            }
        }

    }
}
