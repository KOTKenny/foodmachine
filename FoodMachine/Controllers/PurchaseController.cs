﻿using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodMachine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchaseController : ControllerBase
    {
        private IRepositoryManager _repository;

        public PurchaseController(IRepositoryManager repository)
        {
            _repository = repository;
        }

        [HttpGet("user/{userId}")]
        public IActionResult GetPurchases(int userId)
        {
            var user = _repository.User.GetUser(userId, trackChanges: false);

            if (user == null)
                return NotFound("User not found!");

            var purchases = _repository.Purchase.GetPurchasesByUserId(user.Id, loadProducts: true, trackChanges: false);

            if (purchases == null)
                return NotFound("Purchases not found!");

            return Ok(purchases);
        }
    }
}
