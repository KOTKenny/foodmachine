﻿using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodMachine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SettingController : ControllerBase
    {
        private IRepositoryManager _repository;

        public SettingController(IRepositoryManager repository)
        {
            _repository = repository;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var setting = _repository.Setting.GetSetting(id, trackChanges: false);

            if (setting == null)
                return NotFound();

            return Ok(setting);
        }

        [HttpPut]
        public IActionResult UpdateSetting(int id, [FromBody]Setting setting)
        {
            if (setting == null)
                return BadRequest("Setting object is null");

            var settingEntity = _repository.Setting.GetSetting(id, trackChanges: false);

            if (settingEntity == null)
                return NotFound();

            setting.Id = settingEntity.Id;
            _repository.Setting.UpdateSetting(setting);
            _repository.Save();

            return Ok();
        }
    }
}
