﻿using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodMachine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IRepositoryManager _repository;

        public UserController(IRepositoryManager repository)
        {
            _repository = repository;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var user = _repository.User.GetUser(id, trackChanges: false);

            if (user == null)
                return NotFound();

            return Ok(user);
        }

        [HttpPut]
        public IActionResult Update(int id, [FromBody]User user)
        {
            if (user == null)
                return BadRequest("User object is null");

            var userEntity = _repository.User.GetUser(id, trackChanges: false);

            if (userEntity == null)
                return NotFound();

            user.Id = userEntity.Id;
            _repository.User.UpdateUser(user);
            _repository.Save();

            return Ok();
        }
    }
}
