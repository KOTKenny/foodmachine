import { GetProducts } from "./server/requestCore.js"

(function(){
    GetProducts().then(products => {
        products.forEach(product => {
            console.log(product)
            $("#products").append("<div class=\"d-flex justify-content-around mb-2 col\"> \
                <div class=\"card\" style=\"width: 18rem;\"> \
                    <div class=\"glowing\"><img class=\"card-img-bottom mt-3\" src=\"./img/" + product.imgName + "\" style=\"object-fit: contain; height: 150px;\"></div> \
                        <div class=\"card-body\"> \
                            <div class=\"card-title h5\" style=\"text-align: center;\">"+ product.name +"</div> \
                            <p class=\"card-text\" style=\"text-align: center;\">Стоимость: " + product.cost + " рубля</p> \
                            <div class=\"d-flex justify-content-center\"><button type=\"button\" class=\"btn btn-primary\">Купить</button></div> \
                        </div> \
                    </div> \
                </div>")
        });
    })
}())