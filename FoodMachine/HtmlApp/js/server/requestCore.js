import serverInfo from './serverInfo.json' assert { "type": "json" };

export function GetProducts() {
    return $.ajax({
        url: serverInfo.endpoint.concat("/Product"),
        type: "GET",
        beforeSend: function (xhr, settings) { xhr.setRequestHeader('Authorization', 'Bearer '.concat(serverInfo.bearer)); },
    }).then(data => { return data })
}