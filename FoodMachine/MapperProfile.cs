﻿using AutoMapper;
using DAL.Models;
using Microsoft.AspNetCore.Identity;

namespace FoodMachine
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<LoginModel, User>();
        }
    }
}
