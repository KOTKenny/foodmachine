﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodMachine.Migrations
{
    public partial class InitalData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Cost", "CreateDate", "ImgName", "Name" },
                values: new object[] { 1, 1.69m, new DateTime(2021, 7, 5, 19, 11, 55, 733, DateTimeKind.Local).AddTicks(7645), null, "Coca-Cola" });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Cost", "CreateDate", "ImgName", "Name" },
                values: new object[] { 2, 2.52m, new DateTime(2021, 7, 5, 19, 11, 55, 737, DateTimeKind.Local).AddTicks(4617), null, "Lays 'Сметана и зелень'" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
