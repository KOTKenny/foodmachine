﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodMachine.Migrations
{
    public partial class SettingProfilesModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WorkingNow = table.Column<bool>(type: "bit", nullable: false),
                    AcceptingOnePound = table.Column<bool>(type: "bit", nullable: false),
                    AcceptingTwoPounds = table.Column<bool>(type: "bit", nullable: false),
                    AcceptingFivePounds = table.Column<bool>(type: "bit", nullable: false),
                    CountOfOnePounds = table.Column<int>(type: "int", nullable: false),
                    CountOfTwoPounds = table.Column<int>(type: "int", nullable: false),
                    CountOfFivePounds = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 2, 51, 6, 891, DateTimeKind.Local).AddTicks(8467));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 2, 51, 6, 893, DateTimeKind.Local).AddTicks(127));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Settings");

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 7, 5, 19, 11, 55, 733, DateTimeKind.Local).AddTicks(7645));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 7, 5, 19, 11, 55, 737, DateTimeKind.Local).AddTicks(4617));
        }
    }
}
