﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodMachine.Migrations
{
    public partial class Init_Settings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 2, 52, 42, 584, DateTimeKind.Local).AddTicks(6774));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 2, 52, 42, 585, DateTimeKind.Local).AddTicks(8320));

            migrationBuilder.InsertData(
                table: "Settings",
                columns: new[] { "Id", "AcceptingFivePounds", "AcceptingOnePound", "AcceptingTwoPounds", "CountOfFivePounds", "CountOfOnePounds", "CountOfTwoPounds", "WorkingNow" },
                values: new object[] { 1, true, true, true, 0, 0, 0, true });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 2, 51, 6, 891, DateTimeKind.Local).AddTicks(8467));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 2, 51, 6, 893, DateTimeKind.Local).AddTicks(127));
        }
    }
}
