﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodMachine.Migrations
{
    public partial class Init_UserRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 3, 27, 45, 78, DateTimeKind.Local).AddTicks(1226));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 3, 27, 45, 79, DateTimeKind.Local).AddTicks(3966));

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "User" },
                    { 2, "Admin" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CoinsInFM", "CreateDate", "Name", "RoleId" },
                values: new object[] { 1, 0, new DateTime(2021, 7, 14, 3, 27, 45, 82, DateTimeKind.Local).AddTicks(2409), "Илья", 1 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CoinsInFM", "CreateDate", "Name", "RoleId" },
                values: new object[] { 2, 0, new DateTime(2021, 7, 14, 3, 27, 45, 82, DateTimeKind.Local).AddTicks(2905), "Саша", 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 3, 25, 24, 802, DateTimeKind.Local).AddTicks(9916));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 3, 25, 24, 804, DateTimeKind.Local).AddTicks(906));
        }
    }
}
