﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodMachine.Migrations
{
    public partial class SettingsModelEditCoinsModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AcceptingFivePounds",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "AcceptingOnePound",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "AcceptingTwoPounds",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "CountOfFivePounds",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "CountOfOnePounds",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "CountOfTwoPounds",
                table: "Settings");

            migrationBuilder.CreateTable(
                name: "Coins",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nominal = table.Column<int>(type: "int", nullable: false),
                    Amount = table.Column<int>(type: "int", nullable: false),
                    Accept = table.Column<bool>(type: "bit", nullable: false),
                    SettingId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coins", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Coins_Settings_SettingId",
                        column: x => x.SettingId,
                        principalTable: "Settings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 7, 16, 0, 10, 49, 420, DateTimeKind.Local).AddTicks(1725));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 7, 16, 0, 10, 49, 421, DateTimeKind.Local).AddTicks(5519));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 7, 16, 0, 10, 49, 424, DateTimeKind.Local).AddTicks(588));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 7, 16, 0, 10, 49, 424, DateTimeKind.Local).AddTicks(1092));

            migrationBuilder.CreateIndex(
                name: "IX_Coins_SettingId",
                table: "Coins",
                column: "SettingId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Coins");

            migrationBuilder.AddColumn<bool>(
                name: "AcceptingFivePounds",
                table: "Settings",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AcceptingOnePound",
                table: "Settings",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AcceptingTwoPounds",
                table: "Settings",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "CountOfFivePounds",
                table: "Settings",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CountOfOnePounds",
                table: "Settings",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CountOfTwoPounds",
                table: "Settings",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 3, 27, 45, 78, DateTimeKind.Local).AddTicks(1226));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 3, 27, 45, 79, DateTimeKind.Local).AddTicks(3966));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AcceptingFivePounds", "AcceptingOnePound", "AcceptingTwoPounds" },
                values: new object[] { true, true, true });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 3, 27, 45, 82, DateTimeKind.Local).AddTicks(2409));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 7, 14, 3, 27, 45, 82, DateTimeKind.Local).AddTicks(2905));
        }
    }
}
