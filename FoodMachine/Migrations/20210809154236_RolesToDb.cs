﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodMachine.Migrations
{
    public partial class RolesToDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "58908219-5602-4938-80ed-74ddcebcc7ba", "User", "Пользователь" },
                    { 2, "99f57eda-302d-435b-bd96-28cc5f58e96f", "Admin", "Администратор" }
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreateDate" },
                values: new object[] { "d4320f4d-c80b-40ec-bb9e-a75457bd558d", new DateTime(2021, 8, 9, 18, 42, 36, 131, DateTimeKind.Local).AddTicks(7843) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreateDate" },
                values: new object[] { "6ddc2d29-f250-4945-8a57-bd92f97af720", new DateTime(2021, 8, 9, 18, 42, 36, 131, DateTimeKind.Local).AddTicks(8155) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 8, 9, 18, 42, 36, 129, DateTimeKind.Local).AddTicks(1045));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 8, 9, 18, 42, 36, 130, DateTimeKind.Local).AddTicks(2093));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreateDate" },
                values: new object[] { "d1ae8488-cbe0-486a-91da-0a8053fab3f2", new DateTime(2021, 8, 9, 17, 36, 56, 408, DateTimeKind.Local).AddTicks(2958) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreateDate" },
                values: new object[] { "81569256-1c91-47fe-b139-21a223836691", new DateTime(2021, 8, 9, 17, 36, 56, 408, DateTimeKind.Local).AddTicks(3293) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 8, 9, 17, 36, 56, 405, DateTimeKind.Local).AddTicks(3884));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 8, 9, 17, 36, 56, 406, DateTimeKind.Local).AddTicks(6296));
        }
    }
}
