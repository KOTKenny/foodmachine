﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodMachine.Migrations
{
    public partial class UpdateEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Count",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<decimal>(
                name: "CoinsInFM",
                table: "AspNetUsers",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "a466dc0d-163d-4ba2-86a7-fb60dc8e7a2c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "63aea91d-2c49-4ddb-b21c-e7200ccc05e5");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CoinsInFM", "ConcurrencyStamp", "CreateDate" },
                values: new object[] { 0m, "678c0f26-5a53-48b9-bf5a-b089bccaf7ba", new DateTime(2023, 6, 9, 5, 25, 20, 944, DateTimeKind.Local).AddTicks(8282) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CoinsInFM", "ConcurrencyStamp", "CreateDate" },
                values: new object[] { 0m, "bcdd4ddc-b48d-4431-9ad1-1d00f214f7bc", new DateTime(2023, 6, 9, 5, 25, 20, 944, DateTimeKind.Local).AddTicks(8949) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2023, 6, 9, 5, 25, 20, 938, DateTimeKind.Local).AddTicks(8385));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2023, 6, 9, 5, 25, 20, 941, DateTimeKind.Local).AddTicks(4860));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Count",
                table: "Products");

            migrationBuilder.AlterColumn<int>(
                name: "CoinsInFM",
                table: "AspNetUsers",
                type: "int",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "58908219-5602-4938-80ed-74ddcebcc7ba");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "99f57eda-302d-435b-bd96-28cc5f58e96f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CoinsInFM", "ConcurrencyStamp", "CreateDate" },
                values: new object[] { 0, "d4320f4d-c80b-40ec-bb9e-a75457bd558d", new DateTime(2021, 8, 9, 18, 42, 36, 131, DateTimeKind.Local).AddTicks(7843) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CoinsInFM", "ConcurrencyStamp", "CreateDate" },
                values: new object[] { 0, "6ddc2d29-f250-4945-8a57-bd92f97af720", new DateTime(2021, 8, 9, 18, 42, 36, 131, DateTimeKind.Local).AddTicks(8155) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreateDate",
                value: new DateTime(2021, 8, 9, 18, 42, 36, 129, DateTimeKind.Local).AddTicks(1045));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreateDate",
                value: new DateTime(2021, 8, 9, 18, 42, 36, 130, DateTimeKind.Local).AddTicks(2093));
        }
    }
}
